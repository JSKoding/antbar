import React from 'react';
import { Text } from 'react-native';
import { AppLoading, Font } from 'expo'
import { Icon, TabBar } from 'antd-mobile';

export default class App extends React.Component {
  state = {
    isReady: false,
    selectedTab: 'blueTab'

  }
  async componentDidMount() {
    await Font.loadAsync({
      'anticon': require('./assets/fonts/anticon.ttf')
    });

    this.setState({ isReady: true })
  }
  renderContent = () => (
    <Text>Content</Text>
  )
  renderContent2 = () => (
    <Text>Content2</Text>
  )
  renderContent3 = () => (
    <Text>Content3</Text>
  )
  render() {
    if (!this.state.isReady) {
      return <AppLoading />
    }
    return (
      <TabBar
          unselectedTintColor="#949494"
          tintColor="#33A3F4"
          barTintColor="white"
        >
        <TabBar.Item
          title="Demo1"
          onPress={() => this.setState({ selectedTab: 'blueTab' })}
          selected={this.state.selectedTab === 'blueTab'}
          icon={<Icon name="check" />}
          key="口碑">
          {this.renderContent()}
        </TabBar.Item>
        <TabBar.Item
          title="Demo2"
            onPress={() => this.setState({ selectedTab: 'greenTab' })}
          selected={this.state.selectedTab === 'greenTab'}
          icon={<Icon name="check" />}
          key="口碑">
          {this.renderContent2()}
        </TabBar.Item>
        <TabBar.Item
          title="Demo3"
          onPress={() => this.setState({ selectedTab: 'yellowTab' })}
          selected={this.state.selectedTab === 'yellowTab'}
          icon={<Icon name="check" />}
          key="口碑">
          {this.renderContent3()}
        </TabBar.Item>
      </TabBar>
    );
  }
}
